function [ output_args ] = twinpeaks_startplot(data,peaks)
%plotter for checking start parameters against data before twinpeaks fitting
% Michael L. Baker



if isnumeric(data)
    xData=data(:,1); yData = data(:,2);
elseif isstruct(data)
    alldata = importdata(data.filename);
    [~,data_min_index] = min(abs(alldata(:,1)-data.base.datamin));
    [~,data_max_index] = min(abs(alldata(:,1)-data.base.datamax));
    xData = alldata(data_min_index:data_max_index,1); 
    yData = alldata(data_min_index:data_max_index,2);
elseif isstring(data)
    alldata = importdata(data);
    xData=alldata(:,1); yData = alldata(:,2);
else
    error('first input is not correct')
end    

param_per_peak = numel(peaks.startp)/peaks.number;

%for pseudovoigt convert input from amplitude to area
switch lower(peaks.type)
    case 'pseudovoigt'
        if isfield(peaks,'startp_mode')
            switch peaks.startp_mode
                case 'height'
                    for ii=1:peaks.number
                        xxx = [peaks.startp(ii,1) - peaks.startp(ii,2).*5: 0.05 :  peaks.startp(ii,1) + peaks.startp(ii,2).*5];
                        yyy = PseudoVoigt_multi(xxx,peaks.startp(ii,:),1);
                        yyy = yyy./max(yyy).*peaks.startp(ii,param_per_peak);
                        peaks.startp(ii,end) = trapz(xxx,yyy);
                    end
            end
        end
end

startp = peaks.startp';
parameters = startp(:)';

results = peaks.startp;

% plot data
fig1 = figure;
axes1 = axes('Parent',fig1,...
    'Position',[0.13 0.45 0.775 0.5]);
hold(axes1,'on');

title(['Start Fit with ',num2str(numel(startp)/param_per_peak),' ',peaks.type,' peaks']);


plot(xData,yData,'.'); hold on
peak2plot = 1;

% plot individual peaks

for ii=1:peaks.number
    switch lower(peaks.type)
        case 'pearson7'
            yfit = Pearson7(xData,parameters(peak2plot:peak2plot+param_per_peak-1));
        case 'pseudovoigt'
            yfit = PseudoVoigt_multi(xData,parameters(peak2plot:peak2plot+param_per_peak-1));
            
    end
    plot(xData,yfit,'linewidth',1.5); hold on
    peak2plot = peak2plot + param_per_peak;
    legend_label{ii} = num2str(round(results(ii,1),2));
    
end

% plot total fit
switch lower(peaks.type)
    case 'pearson7'
        plot(xData,Pearson7(xData,parameters),'r','linewidth',1.5); hold on
        residual = yData - Pearson7(xData,parameters);
    case 'pseudovoigt'
        plot(xData,PseudoVoigt_multi(xData,parameters,peaks.number),'r','linewidth',1.5); hold on
        residual = yData - PseudoVoigt_multi(xData,parameters,peaks.number);
end

plot(xData,residual-max(residual),'k');
legend('data',legend_label{:},'total','residual');
axis tight

axes2 = axes('Parent',fig1,'Position',[0.13 0.11 0.775 0.27]);
hold(axes2,'on');


% plot derivative
plot(xData(1:end-1),diff(yData),'.'); hold on
switch lower(peaks.type)
    case 'pearson7'
        plot(xData(1:end-1),diff(Pearson7(xData,parameters)),'r','linewidth',1.5); hold on
    case 'pseudovoigt'
        plot(xData(1:end-1),diff(PseudoVoigt_multi(xData,parameters,peaks.number)),'r','linewidth',1.5); hold on
end
axis tight
