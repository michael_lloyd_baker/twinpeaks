%% Twinpeaks template fitting script
% Michael L. Baker July 22 2017
clear all; close all

% type: help twinpeaks for useage info 

%% load data into matlab and fit baseline 
% data and baseline inputs:
datafile = 'CuI-Cl.txt'; % example data file
base.type = 'spline';
base.smooth = 0.1;
base.peakmin = 8949; 
base.peakmax = 9014;
base.datamin = 8920;
base.datamax = 10000;
base.guess_fwhm = 2;

[xData,yData,basefit] =  baseline(datafile,base);
%% Setup peak fitting, plot inital guess parameters
% peak fitting parameters
peaks.number = 8; % how many peaks
%start param     E  FWHM height
peaks.startp = [8954 2 2 0.073 ;...
                8962 2 2 0.42     ;...
                8972 2 2 4.013      ;...
                8976 2 2 3.164  ;...
                8980 2 2 1.434  ;...
                8989 2 2 0.3745 ;...
                8998 2 2 0.079 ;...
                9007 2 2 0.04];
peaks.background = basefit;
% peak function PseudoVoigt or pearson7 (could add full convolution later)        
peaks.type = 'PseudoVoigt';
% bounds 
peaks.lb(:,1) = peaks.startp(:,1) -1; %energy
peaks.ub(:,1) = peaks.startp(:,1) +1; %energy
peaks.lb(:,2) = peaks.startp(:,2) -1; %width
peaks.ub(:,2) = peaks.startp(:,2) +1; %width
peaks.lb(:,3) = peaks.startp(:,2) -1; %width
peaks.ub(:,3) = peaks.startp(:,3) +2; %width
peaks.lb(:,4) = peaks.startp(:,4).* 0.5; % height
peaks.ub(:,4) = peaks.startp(:,4).* 2.0; % height

twinpeaks_startplot([xData,yData],peaks)
%% twinpeaks fitting 

peaks.algorithm = 'trust-region-reflective';%'levenberg-marquardt';%'trust-region-reflective';
[fitparameters] = twinpeaks([xData,yData],peaks);

% use the results to do another fit, until sum squared residual flats out
peaks.startp = fitparameters;
[fitparameters] = twinpeaks([xData,yData],peaks);


%% Plot the final result including background
twinpeaks_endplot(datafile,base,peaks,basefit,fitparameters)
