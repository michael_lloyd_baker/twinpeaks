%function [xData,peaks,thefit] = baseline(datafile,base)
%Fit the rising edge or baseline for spectroscopy
%baseline(datafile,base)
% datafile is a two colum asci file
% base is a structure with following fields:
% base.datamin = lower energy limit 
% base.datamax = upper energy limit
% base.peakmin = start point of energies to exclude
% base.peakmax = end   point of energies to exclude
% base.type = background fucntion 'interp' 'spline' 'Pearson7'
% 'PseudoVoigt' (both lorent and gauss FWHM) or 'PseudoVoigt3' (one FWHM
% given)
% base.algorithm = trust region reflective (defalt) or 'Levenberg-Marquardt'
% base.startp = optional start parameters for 'Pearson7' or 'PseudoVoigt'
% base.smooth = smoothness of spline fit (defalt 0.95) range 0 to 1
% base.guess_FWHM = guess of average peak width (defalt 2)
% Michael L Baker

