function yy = PseudoVoigt_multi(xdata,param,numpeaks)
% PseudoVoigt(x,xPos,[FWHMG,FWHML,AREA],numpeaks)
% PseudoVoigt(x,xPos,[FWHMG,FWHML,AREA],[param_per_peak])
% PseudoVoigt(x,xPos,[FWHMG*(2 .* sqrt(2*log(2)),FWHML/2,AMP],numpeaks) using ttmult
% plotter input parameters.
% The PseudoVoigt aproximation of lorentzian gaussian convolution.
% The plotter in TTMultiplet package uses gamma for lorentzian line width and sigma for gaussian
% TTmult plotter file input is:
% sigma = fwhm./ (2 .* sqrt(2*log(2)) )
% 2*Gamma
% here we use gaussian FWHM and lorentzian gamma as the inputs.
% See also PseudoVoigt_2pcore for L edges
% Michael L. Baker

if ~exist('numpeaks','var')
    numpeaks = 1;
end

%param_array_size = size(param);
%if param_array_size(2) == 3
%    param = reshape(param',1,numel(param));
%end
yy = zeros(size(xdata));


if numel(numpeaks) == 1
    param_per_peak = ones(1,numpeaks).*numel(param)/numpeaks;
else
    param_per_peak =  numpeaks;
    numpeaks = numel(numpeaks);
end

param_index = 1;
for ii=1:numpeaks
    if param_per_peak(ii) == 4
        xPos = param(param_index);
        FWHMG = abs(param(param_index+1));
        FWHML = abs(param(param_index+2));
        AREA = abs(param(param_index+3));
        param_index = param_index + 4;
    elseif param_per_peak(ii) == 3
        xPos = param(param_index);
        FWHMG = abs(param(param_index+1));
        FWHML = abs(param(param_index+1));
        AREA = abs(param(param_index+2));
        param_index = param_index + 3;
    else
        error('PseudoVoigt_multi.m Input Error')
    end

    f = (FWHMG^5 + 2.69269*(FWHMG^4)*FWHML + 2.42843*(FWHMG^3)*FWHML^2 + 4.47163*(FWHMG^2)*FWHML^3 + 0.07842*FWHMG*FWHML^4 + FWHML^5)^0.2;
    n = 1.36603*(FWHML/f) - 0.47719*(FWHML/f) + 0.11116*(FWHML/f)^3;
    yy = (lorentz(xdata, f, xPos, AREA).*n + gauss(xdata, f, xPos, AREA).*(1-n)) + yy;
    
end

    function y = lorentz(x, FWHMlorentz, xPos, Alorentz)
        y = Alorentz / pi * ( (0.5 * FWHMlorentz ) ./ ( (x-xPos).^2 + (0.5* FWHMlorentz )^2 ) );
    end

    function y = gauss(x, FWHMgauss, xPos, Agauss)
        sigma = FWHMgauss./ (2 .* sqrt(2*log(2)) );
        y = Agauss ./ (sigma .* sqrt(2.*pi)) .* exp( -(x-xPos).^2 ./ (2 .* sigma.^2) );
    end
end