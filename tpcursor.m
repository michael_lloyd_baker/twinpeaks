function data_points = tpcursor(figure_handle,FWHM)


cursor_type = 'normal_cursor';


dcm_obj = datacursormode(figure_handle); set(dcm_obj,'UpdateFcn',eval(['@',cursor_type]))

data_points = figure_handle.CurrentPoint;


    function output_txt = normal_cursor(obj,event_obj)
        
        pos = get(event_obj,'Position');
        output_txt = {['E: ',num2str(pos(1),6)],...
            ['H: ',num2str(pos(2),4)],...
            ['A: ',num2str(pos(2)*FWHM,4)]};
        
    end

end



