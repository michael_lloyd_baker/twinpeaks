function yy = PseudoVoigt_multi(x,xPos,FWHMG,FWHML,AMP)
% PseudoVoigt(x,xPos,FWHMG,FWHML,AMP)
% PseudoVoigt(x,xPos,FWHMG*(2 .* sqrt(2*log(2)),FWHML/2,AMP) using ttmult
% plotter input parameters.
% The PseudoVoigt aproximation of lorentzian gaussian convolution.
% The plotter in TTMultiplet package uses gamma for lorentzian line width and sigma for gaussian
% TTmult plotter file input is:
% sigma = fwhm./ (2 .* sqrt(2*log(2)) )
% 2*Gamma
% here we use gaussian FWHM and lorentzian gamma as the inputs.
% See also PseudoVoigt_2pcore for L edges
% Michael L. Baker

f = (FWHMG^5 + 2.69269*(FWHMG^4)*FWHML + 2.42843*(FWHMG^3)*FWHML^2 + 4.47163*(FWHMG^2)*FWHML^3 + 0.07842*FWHMG*FWHML^4 + FWHML^5)^0.2;
n = 1.36603*(FWHML/f) - 0.47719*(FWHML/f) + 0.11116*(FWHML/f)^3;

yy = lorentz(x, f, xPos, AMP).*n + gauss(x, f, xPos, AMP).*(1-n);

    function y = lorentz(x, FWHMlorentz, xPos, AMPlorentz)
        y = AMPlorentz / pi * ( (0.5 * FWHMlorentz ) ./ ( (x-xPos).^2 + (0.5* FWHMlorentz )^2 ) );
    end

    function y = gauss(x, FWHMgauss, xPos, AMPgauss)
        sigma = FWHMgauss./ (2 .* sqrt(2*log(2)) );
        y = AMPgauss ./ (sigma .* sqrt(2.*pi)) .* exp( -(x-xPos).^2 ./ (2 .* sigma.^2) );
    end
end