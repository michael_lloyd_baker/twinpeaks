function twinpeaks_endplot(datafile,base,peaks,basefit,fitparameters)
%plotter for checking end parameters against data after twinpeaks fitting
% Michael L. Baker

data = importdata(datafile);
[~,data_min_index] = min(abs(data(:,1)-base.datamin));
[~,data_max_index] = min(abs(data(:,1)-base.datamax));

xdata = data(data_min_index:data_max_index,1);
ydata = data(data_min_index:data_max_index,2);


switch lower(peaks.type)
    case 'pseudovoigt'
        if isfield(peaks,'startp_mode')
            switch peaks.startp_mode
                case 'height'
                    for ii=1:peaks.number
                        xxx = [fitparameters(ii,1) - fitparameters(ii,2).*10: 0.05 :  fitparameters(ii,1) + fitparameters(ii,2).*10];
                        yyy = PseudoVoigt_multi(xxx,fitparameters(ii,:),1);
                        yyy = yyy./max(yyy).*fitparameters(ii,end);
                        fitparameters(ii,end) = trapz(xxx,yyy);
                    end
            end
        end
end

fitparameters_array = reshape(fitparameters',1,numel(fitparameters));

figure;
plot(xdata,ydata,'.'); hold on
switch lower(peaks.type)
    case 'pearson7'
        plot(xdata,basefit(xdata)+Pearson7(xdata,fitparameters_array),'r');
    case 'pseudovoigt'
        plot(xdata,basefit(xdata)+PseudoVoigt_multi(xdata,fitparameters_array,peaks.number),'r')
end


xlabel('Energy (eV)','fontsize',14);
set(gca  ,'FontSize'   , 12);
title('total fit')


