function y= Pearson7(xdata,param)
% Pearson7 multiple peak convolution
% input format:
%Pearson7(xdata,xPos,width,AMP) or for multiple peaks
%Pearson7(xdata,xPos1,width1,AMP1,xPos2,width2,AMP2,xPos3,width3,AMP3,etc)

mix = 5;

numpeaks = numel(param)/3;

% if isinteger(numpeaks)
%     error('inputparameter error');
% end

param_index = 1;
y = zeros(size(xdata));
for ii=1:numpeaks
    xPos = param(param_index);
    width = param(param_index+1);
    AMP = abs(param(param_index+2));
    param_index = param_index + 3;
    
    part1=1+4*((xdata-xPos)./width).^2*(2^(1/mix)-1);
    y = y + AMP./(part1.^mix);
end
% 
% function y= Pearson7(varargin)
% % Pearson7 multiple peak convolution
% % input format:
% %Pearson7(xdata,xPos,width,AMP) or for multiple peaks
% %Pearson7(xdata,xPos1,width1,AMP1,xPos2,width2,AMP2,xPos3,width3,AMP3,etc)
% 
% mix = 5;
% 
% xdata = varargin{1};
% numpeaks = (nargin-1)/3
% 
% param_index = 2;
% y = zeros(size(xdata));
% for ii=1:numpeaks
%     xPos = varargin{param_index}
%     width = varargin{param_index+1}
%     AMP = varargin{param_index+2}
%     param_index = param_index + 3;
%     
%     part1=1+4*((xdata-xPos)./width).^2*(2^(1/mix)-1);
%     y = y + AMP./(part1.^mix);
% end

% function y= Pearson7(xdata,xPos,width,AMP)
% mix = 5;
% 
% part1=1+4*((xdata-xPos)./width).^2*(2^(1/mix)-1);
% y=AMP./(part1.^mix);
% 
% end
