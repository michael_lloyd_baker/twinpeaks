%function [results_out,resnorm,residual,output] = twinpeaks(data,peaks)
% twinpeaks(xdata,ydata,peaks)
% a fitting function for spectoscopy
% xdata and ydata are your energy and intensity arrays
% peaks is a structure containing information about the peaks that you
% would like to fit.
%
% peaks.number = number of peaks to fit
% peaks. startp = N x peaks.number matrix of input parameters
% peaks.lb =  N x peaks.number matrix of lower bounds to input parameters
% peaks.ub =  N x peaks.number matrix of upper bounds to input parameters
% peaks.algorithm = 'trust-region-reflective' (for use with lb and ub
%               or  'levenberg-marquardt' for no bounds
%                    (trust-region-reflective is defalt)
% peaks.background = 'no' if background was subtracted (no is defalt) 
% peaks.background = basefit; (result of background fitting)
% peaks.startp_mode = defalt 'area', option 'height'. 
% peaks.background = basefit (result of baseline fitting)
%
% function returns fit parameters [energy FWHM peak_height] or [energy gaussFWHM   lorentz_gamma peak_height]
% Michael L. Baker (bakerml@stanford.edu)
% "The owls are not what they seem"
